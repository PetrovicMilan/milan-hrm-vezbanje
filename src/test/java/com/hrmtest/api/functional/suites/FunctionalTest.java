package com.hrmtest.api.functional.suites;

import com.hrmtest.api.calls.AccountApi;
import com.hrmtest.api.common.CommonResponse;
import com.hrmtest.api.common.init.TestBase;
import com.hrmtest.api.data.dataproviders.StatusDataProvider;
import com.hrmtest.api.data.model.project.pm.EngagementRequest;
import com.hrmtest.api.data.model.project.pm.EngagementResponse;
import com.hrmtest.api.data.model.worklog.changestatus.ChangeStatusRequest;
import com.hrmtest.api.data.model.worklog.changestatus.ChangeStatusResponse;
import com.hrmtest.api.functional.asserts.CommonAssert;
import com.hrmtest.api.functional.asserts.WorklogAssert;
import io.qameta.allure.Description;
import org.testng.annotations.Test;

import java.util.Collections;

// each test class should extend TestBase in order to inherit all common behaviors and to be logged properly in report
public class FunctionalTest extends TestBase {

    // each test case should be simple, readable, in few lines and without any to technical steps. It should represent
    // user actions like as those are done manually on UI
    @Test(groups = {"regression", "smoke"}, dataProvider = "changeStatus", dataProviderClass = StatusDataProvider.class)
    @Description("Change all available statuses")
    // TODO Separate all statuses. Make a separate test for each status
    public static void verifyChangeStatus(String metodNameSuffix, ChangeStatusRequest changeStatusRequest) {

        logStep("INFO: Change status");
        CommonResponse changeStatusActual = AccountApi.changeStatus(token, changeStatusRequest);
        logStep("PASS: Status is changed");
        //TODO Check GetStatus endpoint for statuses instead of checking response
        CommonResponse changeStatusExpected = new CommonResponse();
        changeStatusExpected.parseCommonResponse(0, null, 0, null);

        CommonAssert commonAssert = new CommonAssert();
        logStep("INFO: Verify changed status");
        commonAssert.assertCommonResponse(changeStatusActual, changeStatusExpected);
        logStep("PASS: Changed status is verified");
    }

    @Test(groups = {"regression", "smoke"})
    @Description("Edit current engagement of PM")
    public static void updatePmEngagements() {
        EngagementRequest data = new EngagementRequest(Collections.singletonList(492), 492);

        logStep("INFO: Create ActualPmEngagement");
        EngagementResponse ActualPmEngagement = AccountApi.updatePmEngagement(token, data,92);
        logStep("PASS: ActualPmEngagement is created");

        EngagementResponse ExpectedPmEngagement = new EngagementResponse(0, null, 0, null);

        WorklogAssert worklogAssert = new WorklogAssert();
        logStep("INFO: Verify created PM engagement");
        worklogAssert.assertUpdatePM(ActualPmEngagement, ExpectedPmEngagement);
        logStep("PASS: PM engagement is verified");
    }
}
