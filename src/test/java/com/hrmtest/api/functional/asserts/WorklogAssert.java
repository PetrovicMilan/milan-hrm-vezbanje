package com.hrmtest.api.functional.asserts;

import com.hrmtest.api.common.CommonResponse;
import com.hrmtest.api.data.model.project.pm.EngagementResponse;
import com.hrmtest.api.data.model.worklog.changestatus.ChangeStatusResponse;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.Random;

public class WorklogAssert {
    private SoftAssert softAssert;

    public WorklogAssert() {
        this.softAssert = new SoftAssert();
    }

    public void assertUpdatePM(EngagementResponse actualEngagement, EngagementResponse expectedEngagement) {
        if (actualEngagement == null) {
            Assert.fail("Engagement response was not changed");
        }
        this.softAssert.assertEquals(actualEngagement.getResponseCode(), expectedEngagement.getResponseCode(), "Response code didn't match");
        this.softAssert.assertEquals(actualEngagement.getResponseMessage(), expectedEngagement.getResponseMessage(), "Message didn't match");
        this.softAssert.assertEquals(actualEngagement.getData(), expectedEngagement.getData(), "Data ID didn't match");
        this.softAssert.assertAll();
    }

    public static int getID(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
    public static int getBranch(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}