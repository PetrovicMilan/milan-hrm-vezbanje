package com.hrmtest.api.functional.asserts;

import com.hrmtest.api.common.CommonResponse;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class CommonAssert {
    private SoftAssert softAssert;

    public CommonAssert() {
        this.softAssert = new SoftAssert();
    }

    public void assertCommonResponse(CommonResponse actualResponse, CommonResponse commonResponse) {
        if (actualResponse == null) {
            Assert.fail("Status response was not changed");
        }
        this.softAssert.assertEquals(actualResponse.getResponseCode(), commonResponse.getResponseCode(), "Response code didn't match");
        this.softAssert.assertEquals(actualResponse.getResponseMessage(), commonResponse.getResponseMessage(), "Message didn't match");
        this.softAssert.assertEquals(actualResponse.getData(), commonResponse.getData(), "Data ID didn't match");
        this.softAssert.assertAll();
    }
}
