package com.hrmtest.api.integration.suites;

import com.hrmtest.api.calls.PeopleApi;
import com.hrmtest.api.common.init.TestBase;
import com.hrmtest.api.integration.asserts.CommonAssert;
import io.qameta.allure.Description;
import org.testng.annotations.Test;


public class PeopleTests extends TestBase {
    @Test(groups = {"integration"})
    @Description("Integration test for employee logged in response")
    public static void verifyEmployeeLoggedInResponse() {
        logStep("INFO: Verify Employee Logged In response");
        new CommonAssert().assertResponseStructure(PeopleApi.validateLoggedInUserResponse(token));
        logStep("PASS: Employee Logged In Response is verified");
    }
}
