package com.hrmtest.api.integration.suites;

import com.hrmtest.api.calls.TeamsApi;
import com.hrmtest.api.common.init.TestBase;
import com.hrmtest.api.integration.asserts.CommonAssert;
import io.qameta.allure.Description;
import org.testng.annotations.Test;

public class TeamsTests extends TestBase{
    @Test(groups = {"integration"})
    @Description("Integration test for teams response")
    public static void verifyTeamsResponse() {
        logStep("INFO: Verify teams response");
        new CommonAssert().assertResponseStructure(TeamsApi.validateTeamsResponse(token));
        logStep("PASS: Teams response is verified");
    }
    @Test(groups = {"integration"})
    @Description("Integration test for teams filter response")
    public static void verifyTeamsFiltersResponse() {
        logStep("INFO: Verify teams filters response");
        new CommonAssert().assertResponseStructure(TeamsApi.validateTeamsFilterResponse(token));
        logStep("PASS: Teams filter response is verified");
    }
}
