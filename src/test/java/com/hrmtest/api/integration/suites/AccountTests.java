package com.hrmtest.api.integration.suites;

import com.hrmtest.api.calls.AccountApi;
import com.hrmtest.api.common.init.TestBase;
import com.hrmtest.api.integration.asserts.CommonAssert;
import io.qameta.allure.Description;
import org.testng.annotations.Test;


public class AccountTests extends TestBase {

    @Test(groups = {"integration"})
    @Description("Integration test for claims")
    public static void verifyClaimsAndMe() {
        logStep("INFO: Verify Claims and me");
        new CommonAssert().assertResponseStructure(AccountApi.validateClaimsAndMeResponse(token));
        logStep("PASS: Claims and me are verified");
    }
    @Test(groups = {"integration"})
    @Description("Integration test for mobile claims")
    public static void verifyClaimsAndMeMob() {
        logStep("INFO: Verify Claims and me mobile");
        new CommonAssert().assertResponseStructure(AccountApi.validateClaimsAndMeMobResponse(token));
        logStep("PASS: Claims and me mobile are verified");
    }
    @Test(groups = {"integration"})
    @Description("Integration test for permissions")
    public static void verifyPermissionsAndMe() {
        logStep("INFO: Verify Permissions and me");
        new CommonAssert().assertResponseStructure(AccountApi.validatePermissionsAndMeResponse(token));
        logStep("PASS: Permissions and me are verified");
    }
}
