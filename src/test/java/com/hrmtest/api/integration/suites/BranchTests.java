package com.hrmtest.api.integration.suites;

import com.hrmtest.api.calls.BranchesApi;
import com.hrmtest.api.common.init.TestBase;
import com.hrmtest.api.integration.asserts.CommonAssert;
import io.qameta.allure.Description;
import org.testng.annotations.Test;


public class BranchTests extends TestBase {
    @Test(groups = {"integration"})

    @Description("Integration test for branches")
    public static void verifyBranches() {
        logStep("INFO: Verify branches");
        new CommonAssert().assertResponseStructure(BranchesApi.validateBranchesResponse(token));
        logStep("PASS: Branches are verified");
    }

    @Test(groups = {"integration"})
    @Description("Integration test for branch offices")
    public static void verifyBranchOffices() {
        logStep("INFO: Verify branch offices");
        new CommonAssert().assertResponseStructure(BranchesApi.validateBranchOfficesResponse(token));
        logStep("PASS: Branch offices are verified");
    }
    @Test(groups = {"integration"})
    @Description("Integration test for filters response")
    public static void verifyFiltersResponse() {
        logStep("INFO: Verify filters response");
        new CommonAssert().assertResponseStructure(BranchesApi.validateFiltersResponse(token));
        logStep("PASS: Filters Response is verified");
    }

}
