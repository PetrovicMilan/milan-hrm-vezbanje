
package com.hrmtest.api.integration.suites;

import com.hrmtest.api.calls.AccountApi;
import com.hrmtest.api.calls.WorklogApi;
import com.hrmtest.api.common.init.TestBase;
import com.hrmtest.api.integration.asserts.CommonAssert;
import io.qameta.allure.Description;
import org.testng.annotations.Test;

public class VariousTests extends TestBase {


    @Test(groups = {"integration"})
    @Description("Integration test for engagements types")
    public static void verifyEngagementTypes() {
        logStep("INFO: Verify engagements types");
        new CommonAssert().assertResponseStructure(AccountApi.validateEngagementTypesResponse(token));
        logStep("PASS: Engagements types are verified");
    }

    @Test(groups = {"integration"})
    @Description("Integration test for positions")
    public static void verifyPositions() {
        logStep("INFO: Verify positions");
        new CommonAssert().assertResponseStructure(WorklogApi.validatePositionsResponse(token));
        logStep("PASS: Positions are verified");
    }
}
