package com.hrmtest.api.integration.suites;

import com.hrmtest.api.calls.WorklogApi;
import com.hrmtest.api.common.init.TestBase;
import com.hrmtest.api.integration.asserts.CommonAssert;
import io.qameta.allure.Description;
import org.testng.annotations.Test;

public class WorklogTests extends TestBase {
    @Test(groups = {"integration"})
    @Description("Integration test for worklog correction scope")
    public static void verifyWorklogCorrectionScope() {
        Integer id = 256088;
        logStep("INFO: Verify WL scope");
        new CommonAssert().assertResponseStructure(WorklogApi.validateCorrectionScopeResponse(token, id));
        logStep("PASS: WL scope is verified");
    }

    @Test(groups = {"integration"})
    @Description("Integration test for worklog statuses")
    public static void verifyWorklogStatus() {
        logStep("INFO: Verify status");
        new CommonAssert().assertResponseStructure(WorklogApi.validateStatusResponse(token));
        logStep("PASS: Status is verified");
    }
}
