package com.hrmtest.api.calls;

import com.hrmtest.api.common.GsonFunctions;
import com.hrmtest.api.common.ResponseValidation;
import com.hrmtest.api.common.RestAssuredFunctions;
import com.hrmtest.api.constants.ApiEndpoints;
import com.hrmtest.api.data.model.teams.FiltersResponse;
import com.hrmtest.api.data.model.teams.TeamsResponse;

public class TeamsApi {
    public static ResponseValidation validateTeamsResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.TEAMS).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, TeamsResponse.class);
    }
    public static ResponseValidation validateTeamsFilterResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.TEAMS_FILTERS).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, FiltersResponse.class);
    }
}
