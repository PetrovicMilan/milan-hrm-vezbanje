package com.hrmtest.api.calls;

import com.hrmtest.api.common.GsonFunctions;
import com.hrmtest.api.common.ResponseValidation;
import com.hrmtest.api.common.RestAssuredFunctions;
import com.hrmtest.api.constants.ApiEndpoints;
import com.hrmtest.api.data.model.people.employees.EmployeeLoggedInResponse;

public class PeopleApi {
    public static ResponseValidation validateLoggedInUserResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.EMPLOYEE_ME).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, EmployeeLoggedInResponse.class);
    }
}
