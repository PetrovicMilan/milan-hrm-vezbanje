package com.hrmtest.api.calls;

import com.hrmtest.api.common.GsonFunctions;
import com.hrmtest.api.common.ResponseValidation;
import com.hrmtest.api.common.RestAssuredFunctions;
import com.hrmtest.api.constants.ApiEndpoints;
import com.hrmtest.api.data.model.branches.FiltersResponse;
import com.hrmtest.api.data.model.employments.branchOffices.BranchOfficesResponse;
import com.hrmtest.api.data.model.employments.branches.BranchesResponse;


public class BranchesApi {
    public static ResponseValidation validateBranchesResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.BRANCHES).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, BranchesResponse.class);
    }
    public static ResponseValidation validateBranchOfficesResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.BRANCH_OFFICES).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, BranchOfficesResponse.class);
    }
    public static ResponseValidation validateFiltersResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.FILTERS).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, FiltersResponse.class);
    }


}