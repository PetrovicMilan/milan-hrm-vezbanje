package com.hrmtest.api.calls;

import com.hrmtest.api.common.GsonFunctions;
import com.hrmtest.api.common.ResponseValidation;
import com.hrmtest.api.common.RestAssuredFunctions;
import com.hrmtest.api.constants.ApiEndpoints;
import com.hrmtest.api.data.model.employments.positions.PositionsResponse;
import com.hrmtest.api.data.model.worklog.corectionscope.CorrectionScopeResponse;
import com.hrmtest.api.data.model.worklog.curentstatus.StatusResponse;

public class WorklogApi {
    public static ResponseValidation validateCorrectionScopeResponse(String accessToken, Integer id) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.CORRECTION_SCOPE(id)).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, CorrectionScopeResponse.class);
    }

    public static ResponseValidation validateStatusResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.STATUS).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, StatusResponse.class);
    }

    public static ResponseValidation validatePositionsResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.EMPLOYMENTS_POSITIONS).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, PositionsResponse.class);
    }
}
