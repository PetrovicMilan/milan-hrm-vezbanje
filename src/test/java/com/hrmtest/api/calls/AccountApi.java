package com.hrmtest.api.calls;

import com.hrmtest.api.common.GsonFunctions;
import com.hrmtest.api.common.ResponseValidation;
import com.hrmtest.api.common.RestAssuredFunctions;
import com.hrmtest.api.constants.ApiEndpoints;
import com.hrmtest.api.data.model.account.claimsandme.ClaimsAndMeResponse;
import com.hrmtest.api.data.model.account.claimsandme.mob.ClaimsAndMeMobResponse;
import com.hrmtest.api.data.model.account.permisionsandme.PermisionsAndMeResponse;
import com.hrmtest.api.data.model.employments.engagements.EngagementTypesResponse;
import com.hrmtest.api.data.model.project.pm.EngagementRequest;
import com.hrmtest.api.data.model.project.pm.EngagementResponse;
import com.hrmtest.api.data.model.worklog.changestatus.ChangeStatusRequest;
import com.hrmtest.api.data.model.worklog.changestatus.ChangeStatusResponse;


public class AccountApi {
    //      Integration tests
    public static ResponseValidation validateEngagementTypesResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.EMPLOYMENTS_ENGAGEMENTS_TYPES).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, EngagementTypesResponse.class);
    }
//    Account
    public static ResponseValidation validateClaimsAndMeResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.CLAIMS_AND_ME).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, ClaimsAndMeResponse.class);
    }
    public static ResponseValidation validateClaimsAndMeMobResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.CLAIMS_AND_ME_MOB).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, ClaimsAndMeMobResponse.class);
    }
    public static ResponseValidation validatePermissionsAndMeResponse(String accessToken) {
        String jsonResponse = RestAssuredFunctions.get(accessToken, ApiEndpoints.PERMISSIONS_AND_ME).body().asString();
        return GsonFunctions.verifyResponse(jsonResponse, PermisionsAndMeResponse.class);
    }

    // Functional tests
    public static ChangeStatusResponse changeStatus(String accessToken, ChangeStatusRequest createRequest) {
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(createRequest, accessToken, ApiEndpoints.CHANGE_STATUS).body().asString(), ChangeStatusResponse.class);
    }
    public static EngagementResponse updatePmEngagement(String accessToken, EngagementRequest createRequest, Integer projectId) {
        return GsonFunctions.parseSuccessResponseToModel(RestAssuredFunctions.post(createRequest, accessToken, ApiEndpoints.UPDATE_PM_ENGAGEMENTS(projectId)).body().asString(), EngagementResponse.class);
    }
}