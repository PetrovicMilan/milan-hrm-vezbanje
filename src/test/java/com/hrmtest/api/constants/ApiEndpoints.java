package com.hrmtest.api.constants;

public class ApiEndpoints {
    public static final String API = "/api";
    public static final String VERSION = "/v1";
    public static final String VERSION2 = "/v2";
    public static final String VERSION3= "/v3";
    public static final String CORE_API = API + VERSION;
    public static final String CORE_API_VERSION2 = API + VERSION2;
    public static final String CORE_API_VERSION3 = API + VERSION3;

    //      Account
    public static final String ACCOUNT = "/Account";
    public static final String CLAIMS_AND_ME = ACCOUNT + "/claims-and-me";
    public static final String CLAIMS_AND_ME_MOB = CLAIMS_AND_ME + "/mobile";
    public static final String PERMISSIONS_AND_ME = ACCOUNT + "/permissions-and-me";
    //      Branches

    public static final String BRANCH_OFFICES = API + "/BranchOffice/BranchOffices";
    public static final String BRANCHES = CORE_API + "/Branches";
    public static final String FILTERS = BRANCHES + "/Filters";

    //      People
    public static final String PEOPLE = CORE_API_VERSION2 + "/People";
    public static final String EMPLOYEES = PEOPLE + "/Employees";
    public static final String EMPLOYEE_ME = EMPLOYEES + "/me";

    //      Worklog
    public static final String WORKLOG = "/WorkLog";
    public static final String STATUS = API + WORKLOG + "/Status";

    public static final String CORRECTION_SCOPE(Integer worklogId) {
        return API + WORKLOG + "/WorkLogCorrectionScope/" + worklogId;
    }

    public static final String CHANGE_STATUS = API + WORKLOG + "/ChangeStatus";

    //     Employees
    public static final String EMPLOYMENTS_ENGAGEMENTS_TYPES = API + "/Employments/engagement-types";
    public static final String EMPLOYMENTS_POSITIONS = API + "/Employments/positions";

    //     Project
    public static final String UPDATE_PM_ENGAGEMENTS(Integer projectId) {
        return API + "Projects/" + projectId + "/project-managers";
    }

    //Teams
    public static final String TEAMS = CORE_API_VERSION3 + "/Teams";
    public static final String TEAMS_FILTERS = TEAMS + "/Filters";
}
