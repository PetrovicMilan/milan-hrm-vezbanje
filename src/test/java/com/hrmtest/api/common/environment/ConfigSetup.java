package com.hrmtest.api.common.environment;

public class ConfigSetup extends ConfigReader {

    public ConfigSetup() {

    }

    public static String getBaseURL() {
        return getValue("BASE_URL");
    }


    public static String getIdentityUrl() {
        return getValue("BASE_URL_IDENTITY");
    }

    public static String getPassword() {
        return getValue("PASSWORD");
    }

    public static String getEmail() {
        return getValue("EMAIL");
    }

    public static String getDbUser() {
        return getValue("DB_USER");
    }

    public static String getDbPsw() {
        return getValue("DB_PSW");
    }

    public static String getDbUrl() {
        return getValue("DB_URL");
    }

}
