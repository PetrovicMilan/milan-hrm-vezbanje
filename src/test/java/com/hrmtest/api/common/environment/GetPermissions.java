package com.hrmtest.api.common.environment;


import io.restassured.http.ContentType;
import io.restassured.response.Response;


import static io.restassured.RestAssured.given;


public class GetPermissions {

    private static ConfigSetup getAppConfig() {
        return new ConfigSetup();
    }

    public static String getToken() {
        Response resp = given()
                .headers("Content-Type", "application/x-www-form-urlencoded", "Accept", ContentType.JSON)
                .param("grant_type", "password")
                .param("client_id", "ResourceOwner")
                .param("username", getAppConfig().getEmail())
                .param("password", getAppConfig().getPassword())
                .param("client_secret", "owner")
                .when()
                .post(getAppConfig().getIdentityUrl())
                .then()
                .extract()
                .response();
        return resp.jsonPath().getString("access_token");

    }
}
