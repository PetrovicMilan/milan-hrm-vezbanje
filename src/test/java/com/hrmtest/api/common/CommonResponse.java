package com.hrmtest.api.common;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.data.model.common.ErrorResponse;
import com.hrmtest.api.functional.asserts.CommonAssert;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class CommonResponse  extends ErrorResponse implements Serializable
{

    @SerializedName("ResponseCode")
    @Expose
    private Integer ResponseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String ResponseMessage;
    @SerializedName("Data")
    @Expose
    private Integer Data;
    @SerializedName("SpecialData")
    @Expose
    private Object SpecialData;
    private final static long serialVersionUID = 6534340923900447394L;


    public CommonResponse() {
    }

    public CommonResponse(Integer responseCode, String responseMessage, Integer data, Object specialData) {
        super();
        this.ResponseCode = responseCode;
        this.ResponseMessage = responseMessage;
        this.Data = data;
        this.SpecialData = specialData;
    }

    public CommonResponse parseCommonResponse(Integer responseCode, String responseMessage, Integer data, Object specialData) {
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.setResponseCode(responseCode);
        commonResponse.setResponseMessage(responseMessage);
        commonResponse.setData(data);
        commonResponse.setSpecialData(specialData);
        return commonResponse;
    }

    public Integer getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.ResponseCode = responseCode;
    }

    public String getResponseMessage() {
        return ResponseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.ResponseMessage = responseMessage;
    }

    public Integer getData() {
        return Data;
    }

    public void setData(Integer data) {
        this.Data = data;
    }

    public Object getSpecialData() {
        return SpecialData;
    }

    public void setSpecialData(Object specialData) {
        this.SpecialData = specialData;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("responseCode", ResponseCode).append("responseMessage", ResponseMessage).append("data", Data).append("specialData", SpecialData).toString();
    }

}