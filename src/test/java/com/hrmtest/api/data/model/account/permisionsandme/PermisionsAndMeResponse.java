package com.hrmtest.api.data.model.account.permisionsandme;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.data.model.common.ErrorResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PermisionsAndMeResponse extends ErrorResponse implements Serializable {

    @SerializedName("HtecEmail")
    @Expose
    private String HtecEmail;
    @SerializedName("Branch")
    @Expose
    private Branch Branch;
    @SerializedName("LoginStatus")
    @Expose
    private String LoginStatus;
    @SerializedName("Roles")
    @Expose
    private List<Role> Roles = null;
    @SerializedName("Permissions")
    @Expose
    private List<Permission> Permissions = null;
    @SerializedName("Positions")
    @Expose
    private List<Position> Positions = null;
    @SerializedName("OpenLogs")
    @Expose
    private List<Object> OpenLogs;
    @SerializedName("RelatedUsers")
    @Expose
    private List<Integer> RelatedUsers = null;
    @SerializedName("Tutorials")
    @Expose
    private List<Tutorial> Tutorials = null;
    @SerializedName("AvailableStatusIds")
    @Expose
    private List<Integer> AvailableStatusIds = null;
    @SerializedName("ShowBreakModal")
    @Expose
    private Boolean ShowBreakModal;
    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("FirstName")
    @Expose
    private String FirstName;
    @SerializedName("LastName")
    @Expose
    private String LastName;
    @SerializedName("ParentName")
    @Expose
    private String ParentName;
    @SerializedName("Gender")
    @Expose
    private String Gender;
    @SerializedName("Image")
    @Expose
    private String Image;
    @SerializedName("LoginStatusId")
    @Expose
    private Integer LoginStatusId;
    @SerializedName("EmployeeStatus")
    @Expose
    private Integer EmployeeStatus;
    private final static long serialVersionUID = -8383413378347470052L;


    public PermisionsAndMeResponse() {
    }

    public PermisionsAndMeResponse(String htecEmail, Branch branch, String loginStatus, List<Role> roles, List<Permission> permissions, List<Position> positions, List<Object> openLogs, List<Integer> relatedUsers, List<Tutorial> tutorials, List<Integer> availableStatusIds, Boolean showBreakModal, Integer id, String firstName, String lastName, String parentName, String gender, String image, Integer loginStatusId, Integer employeeStatus) {
        super();
        this.HtecEmail = htecEmail;
        this.Branch = branch;
        this.LoginStatus = loginStatus;
        this.Roles = roles;
        this.Permissions = permissions;
        this.Positions = positions;
        this.OpenLogs = openLogs;
        this.RelatedUsers = relatedUsers;
        this.Tutorials = tutorials;
        this.AvailableStatusIds = availableStatusIds;
        this.ShowBreakModal = showBreakModal;
        this.Id = id;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.ParentName = parentName;
        this.Gender = gender;
        this.Image = image;
        this.LoginStatusId = loginStatusId;
        this.EmployeeStatus = employeeStatus;
    }

    public String getHtecEmail() {
        return HtecEmail;
    }

    public void setHtecEmail(String htecEmail) {
        this.HtecEmail = htecEmail;
    }

    public Branch getBranch() {
        return Branch;
    }

    public void setBranch(Branch branch) {
        this.Branch = branch;
    }

    public String getLoginStatus() {
        return LoginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.LoginStatus = loginStatus;
    }

    public List<Role> getRoles() {
        return Roles;
    }

    public void setRoles(List<Role> roles) {
        this.Roles = roles;
    }

    public List<Permission> getPermissions() {
        return Permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.Permissions = permissions;
    }

    public List<Position> getPositions() {
        return Positions;
    }

    public void setPositions(List<Position> positions) {
        this.Positions = positions;
    }

    public List<Object> getOpenLogs() {
        return OpenLogs;
    }

    public void setOpenLogs(List<Object> openLogs) {
        this.OpenLogs = openLogs;
    }

    public List<Integer> getRelatedUsers() {
        return RelatedUsers;
    }

    public void setRelatedUsers(List<Integer> relatedUsers) {
        this.RelatedUsers = relatedUsers;
    }

    public List<Tutorial> getTutorials() {
        return Tutorials;
    }

    public void setTutorials(List<Tutorial> tutorials) {
        this.Tutorials = tutorials;
    }

    public List<Integer> getAvailableStatusIds() {
        return AvailableStatusIds;
    }

    public void setAvailableStatusIds(List<Integer> availableStatusIds) {
        this.AvailableStatusIds = availableStatusIds;
    }

    public Boolean getShowBreakModal() {
        return ShowBreakModal;
    }

    public void setShowBreakModal(Boolean showBreakModal) {
        this.ShowBreakModal = showBreakModal;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        this.Id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        this.FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        this.LastName = lastName;
    }

    public String getParentName() {
        return ParentName;
    }

    public void setParentName(String parentName) {
        this.ParentName = parentName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        this.Gender = gender;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        this.Image = image;
    }

    public Integer getLoginStatusId() {
        return LoginStatusId;
    }

    public void setLoginStatusId(Integer loginStatusId) {
        this.LoginStatusId = loginStatusId;
    }

    public Integer getEmployeeStatus() {
        return EmployeeStatus;
    }

    public void setEmployeeStatus(Integer employeeStatus) {
        this.EmployeeStatus = employeeStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("HtecEmail", HtecEmail).append("Branch", Branch)
                .append("LoginStatus", LoginStatus).append("Roles", Roles).append("Permissions", Permissions)
                .append("Positions", Positions).append("OpenLogs", OpenLogs).append("RelatedUsers", RelatedUsers)
                .append("Tutorials", Tutorials).append("AvailableStatusIds", AvailableStatusIds)
                .append("ShowBreakModal", ShowBreakModal).append("Id", Id).append("FirstName", FirstName)
                .append("LastName", LastName).append("ParentName", ParentName).append("Gender", Gender)
                .append("Image", Image).append("LoginStatusId", LoginStatusId).append("EmployeeStatus", EmployeeStatus)
                .toString();
    }

}