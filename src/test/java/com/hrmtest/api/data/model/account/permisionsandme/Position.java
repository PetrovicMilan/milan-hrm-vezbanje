package com.hrmtest.api.data.model.account.permisionsandme;


import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Position implements Serializable
{

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("ShortName")
    @Expose
    private String ShortName;
    private final static long serialVersionUID = -8881674968331645387L;


    public Position() {
    }


    public Position(Integer Id, String Name, String shortName) {
        super();
        this.Id = Id;
        this.Name = Name;
        this.ShortName = ShortName;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String ShortName) {
        this.ShortName = ShortName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", Id).append("Name", Name).append("ShortName", ShortName).toString();
    }

}