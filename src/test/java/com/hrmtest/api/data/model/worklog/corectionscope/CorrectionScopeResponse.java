package com.hrmtest.api.data.model.worklog.corectionscope;

import java.io.Serializable;

import com.hrmtest.api.annotations.ResponseRequiredField;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.data.model.common.ErrorResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class CorrectionScopeResponse extends ErrorResponse implements Serializable
{
    @ResponseRequiredField(canBeEmpty = false)
    @SerializedName("StartTime")
    @Expose
    private String StartTime;
    @ResponseRequiredField(canBeEmpty = false)
    @SerializedName("EndTime")
    @Expose
    private String EndTime;
    @ResponseRequiredField(canBeEmpty = false)
    @SerializedName("TotalHours")
    @Expose
    private Double TotalHours;
    private final static long serialVersionUID = -5231568421088360198L;

    public CorrectionScopeResponse() {
    }

    public CorrectionScopeResponse(String startTime, String endTime, Double totalHours) {
        super();
        this.StartTime = startTime;
        this.EndTime = endTime;
        this.TotalHours = totalHours;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        this.StartTime = startTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String endTime) {
        this.EndTime = endTime;
    }

    public Double getTotalHours() {
        return TotalHours;
    }

    public void setTotalHours(Double totalHours) {
        this.TotalHours = totalHours;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("StartTime", StartTime).append("EndTime", EndTime).append("TotalHours", TotalHours).toString();
    }

}