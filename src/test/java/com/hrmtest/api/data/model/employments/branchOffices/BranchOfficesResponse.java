package com.hrmtest.api.data.model.employments.branchOffices;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;
import com.hrmtest.api.data.model.common.ErrorResponse;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BranchOfficesResponse extends ErrorResponse implements Serializable
{

    @SerializedName("BranchOfficeId")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer BranchOfficeId;
    @SerializedName("Title")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String Title;
    @SerializedName("Description")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String Description;
    @SerializedName("Location")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String Location;
    @SerializedName("Radius")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private Double Radius;
    @SerializedName("Floor")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private Integer Floor;
    @SerializedName("BranchName")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String BranchName;
    @SerializedName("Latitude")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private Double Latitude;
    @SerializedName("Longitude")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private Double Longitude;
    @SerializedName("OfficeRouters")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private List<OfficeRouter> OfficeRouters = new ArrayList<OfficeRouter>();
    private final static long serialVersionUID = 8672980708556876310L;

    public BranchOfficesResponse() {
    }

    public BranchOfficesResponse(Integer branchOfficeId, String title, String description, String location, Double radius, Integer floor, String branchName, Double latitude, Double longitude, List<OfficeRouter> officeRouters) {
        super();
        this.BranchOfficeId = branchOfficeId;
        this.Title = title;
        this.Description = description;
        this.Location = location;
        this.Radius = radius;
        this.Floor = floor;
        this.BranchName = branchName;
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.OfficeRouters = officeRouters;
    }

    public Integer getBranchOfficeId() {
        return BranchOfficeId;
    }

    public void setBranchOfficeId(Integer branchOfficeId) {
        this.BranchOfficeId = branchOfficeId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public Object getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        this.Location = location;
    }

    public Double getRadius() {
        return Radius;
    }

    public void setRadius(Double radius) {
        this.Radius = radius;
    }

    public Integer getFloor() {
        return Floor;
    }

    public void setFloor(Integer floor) {
        this.Floor = floor;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        this.BranchName = branchName;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        this.Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        this.Longitude = longitude;
    }

    public List<OfficeRouter> getOfficeRouters() {
        return OfficeRouters;
    }

    public void setOfficeRouters(List<OfficeRouter> officeRouters) {
        this.OfficeRouters = officeRouters;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("BranchOfficeId", BranchOfficeId).append("Title", Title).append("Description", Description).append("Location", Location).append("Radius", Radius).append("Floor", Floor).append("BranchName", BranchName).append("Latitude", Latitude).append("Longitude", Longitude).append("OfficeRouters", OfficeRouters).toString();
    }

}