package com.hrmtest.api.data.model.teams;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;

import java.io.Serializable;

public class Branch implements Serializable {

    @SerializedName("BranchId")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer branchId;
    @SerializedName("BranchName")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String branchName;

    public Branch() {
    }

    public Branch(Integer branchId, String branchName) {
        super();
        this.branchId = branchId;
        this.branchName = branchName;
    }

    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Branch.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("branchId");
        sb.append('=');
        sb.append(((this.branchId == null)?"<null>":this.branchId));
        sb.append(',');
        sb.append("branchName");
        sb.append('=');
        sb.append(((this.branchName == null)?"<null>":this.branchName));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}