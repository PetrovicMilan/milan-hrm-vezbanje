package com.hrmtest.api.data.model.common;

public class Statuses {


    public enum UserStatuses {

        ON(1),
        BREAK(2),
        OFF(3),
        WORK_FROM_HOME(4),
        WORK_FROM_FIELD(5);



        private Integer value;


        UserStatuses(Integer value) {

            this.value = value;

        }


        public Integer getValue() {

            return value;

        }

        }
}