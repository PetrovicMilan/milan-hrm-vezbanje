package com.hrmtest.api.data.model.employments.positions;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;
import com.hrmtest.api.data.model.common.ErrorResponse;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PositionsResponse extends ErrorResponse implements Serializable
{

    @SerializedName("Id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer Id;
    @SerializedName("Name")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String Name;
    @SerializedName("ShortName")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String ShortName;
    @SerializedName("PositionTypeId")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer PositionTypeId;
    @SerializedName("PositionTypeName")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private Object PositionTypeName;
    private final static long serialVersionUID = -1412624948096217684L;


    public PositionsResponse() {
    }

    public PositionsResponse(Integer id, String name, String shortName, Integer positionTypeId, Object positionTypeName) {
        super();
        this.Id = id;
        this.Name = name;
        this.ShortName = shortName;
        this.PositionTypeId = positionTypeId;
        this.PositionTypeName = positionTypeName;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        this.Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        this.ShortName = shortName;
    }

    public Integer getPositionTypeId() {
        return PositionTypeId;
    }

    public void setPositionTypeId(Integer positionTypeId) {
        this.PositionTypeId = positionTypeId;
    }

    public Object getPositionTypeName() {
        return PositionTypeName;
    }

    public void setPositionTypeName(Object positionTypeName) {
        this.PositionTypeName = positionTypeName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", Id).append("name", Name).append("shortName", ShortName).append("positionTypeId", PositionTypeId).append("positionTypeName", PositionTypeName).toString();
    }

}