package com.hrmtest.api.data.model.project.pm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.data.model.project.pm.ProjectManager;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class SpecialData implements Serializable
{

    @SerializedName("iD")
    @Expose
    private int iD;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("projectManagers")
    @Expose
    private List<ProjectManager> projectManagers = new ArrayList<ProjectManager>();
    @SerializedName("peopleOnProject")
    @Expose
    private int peopleOnProject;
    @SerializedName("startDate")
    @Expose
    private String startDate;
    @SerializedName("endDate")
    @Expose
    private Object endDate;
    @SerializedName("plannedStart")
    @Expose
    private Object plannedStart;
    @SerializedName("plannedEnd")
    @Expose
    private Object plannedEnd;
    @SerializedName("jiraCode")
    @Expose
    private String jiraCode;
    @SerializedName("isRequestPm")
    @Expose
    private boolean isRequestPm;
    @SerializedName("isRequestProjectPm")
    @Expose
    private boolean isRequestProjectPm;
    @SerializedName("hasPostMembersPermission")
    @Expose
    private boolean hasPostMembersPermission;
    @SerializedName("hasDeleteMembersPermission")
    @Expose
    private boolean hasDeleteMembersPermission;
    @SerializedName("nextEvaluationDate")
    @Expose
    private String nextEvaluationDate;
    private final static long serialVersionUID = 3937719724367656350L;

    public SpecialData() {
    }

    public SpecialData(int iD, String name, String description, List<ProjectManager> projectManagers, int peopleOnProject, String startDate, Object endDate, Object plannedStart, Object plannedEnd, String jiraCode, boolean isRequestPm, boolean isRequestProjectPm, boolean hasPostMembersPermission, boolean hasDeleteMembersPermission, String nextEvaluationDate) {
        super();
        this.iD = iD;
        this.name = name;
        this.description = description;
        this.projectManagers = projectManagers;
        this.peopleOnProject = peopleOnProject;
        this.startDate = startDate;
        this.endDate = endDate;
        this.plannedStart = plannedStart;
        this.plannedEnd = plannedEnd;
        this.jiraCode = jiraCode;
        this.isRequestPm = isRequestPm;
        this.isRequestProjectPm = isRequestProjectPm;
        this.hasPostMembersPermission = hasPostMembersPermission;
        this.hasDeleteMembersPermission = hasDeleteMembersPermission;
        this.nextEvaluationDate = nextEvaluationDate;
    }

    public int getID() {
        return iD;
    }

    public void setID(int iD) {
        this.iD = iD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProjectManager> getProjectManagers() {
        return projectManagers;
    }

    public void setProjectManagers(List<ProjectManager> projectManagers) {
        this.projectManagers = projectManagers;
    }

    public int getPeopleOnProject() {
        return peopleOnProject;
    }

    public void setPeopleOnProject(int peopleOnProject) {
        this.peopleOnProject = peopleOnProject;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public Object getEndDate() {
        return endDate;
    }

    public void setEndDate(Object endDate) {
        this.endDate = endDate;
    }

    public Object getPlannedStart() {
        return plannedStart;
    }

    public void setPlannedStart(Object plannedStart) {
        this.plannedStart = plannedStart;
    }

    public Object getPlannedEnd() {
        return plannedEnd;
    }

    public void setPlannedEnd(Object plannedEnd) {
        this.plannedEnd = plannedEnd;
    }

    public String getJiraCode() {
        return jiraCode;
    }

    public void setJiraCode(String jiraCode) {
        this.jiraCode = jiraCode;
    }

    public boolean isIsRequestPm() {
        return isRequestPm;
    }

    public void setIsRequestPm(boolean isRequestPm) {
        this.isRequestPm = isRequestPm;
    }

    public boolean isIsRequestProjectPm() {
        return isRequestProjectPm;
    }

    public void setIsRequestProjectPm(boolean isRequestProjectPm) {
        this.isRequestProjectPm = isRequestProjectPm;
    }

    public boolean isHasPostMembersPermission() {
        return hasPostMembersPermission;
    }

    public void setHasPostMembersPermission(boolean hasPostMembersPermission) {
        this.hasPostMembersPermission = hasPostMembersPermission;
    }

    public boolean isHasDeleteMembersPermission() {
        return hasDeleteMembersPermission;
    }

    public void setHasDeleteMembersPermission(boolean hasDeleteMembersPermission) {
        this.hasDeleteMembersPermission = hasDeleteMembersPermission;
    }

    public String getNextEvaluationDate() {
        return nextEvaluationDate;
    }

    public void setNextEvaluationDate(String nextEvaluationDate) {
        this.nextEvaluationDate = nextEvaluationDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("iD", iD).append("name", name).append("description", description).append("projectManagers", projectManagers).append("peopleOnProject", peopleOnProject).append("startDate", startDate).append("endDate", endDate).append("plannedStart", plannedStart).append("plannedEnd", plannedEnd).append("jiraCode", jiraCode).append("isRequestPm", isRequestPm).append("isRequestProjectPm", isRequestProjectPm).append("hasPostMembersPermission", hasPostMembersPermission).append("hasDeleteMembersPermission", hasDeleteMembersPermission).append("nextEvaluationDate", nextEvaluationDate).toString();
    }

}
