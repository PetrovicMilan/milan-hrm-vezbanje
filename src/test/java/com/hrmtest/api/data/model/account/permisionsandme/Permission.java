package com.hrmtest.api.data.model.account.permisionsandme;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Permission implements Serializable
{

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Scope")
    @Expose
    private String Scope;
    private final static long serialVersionUID = 7569689585356654411L;

    public Permission() {
    }


    public Permission(Integer Id, String Name, String Scope) {
        super();
        this.Id = Id;
        this.Name = Name;
        this.Scope = Scope;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getScope() {
        return Scope;
    }

    public void setScope(String Scope) {
        this.Scope = Scope;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", Id).append("Name", Name).append("Scope", Scope).toString();
    }

}
