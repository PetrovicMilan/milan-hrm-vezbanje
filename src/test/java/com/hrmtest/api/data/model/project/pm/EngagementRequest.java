package com.hrmtest.api.data.model.project.pm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class EngagementRequest implements Serializable
{

    @SerializedName("projectManagersIds")
    @Expose
    private List<Integer> projectManagersIds = new ArrayList<Integer>();
    @SerializedName("projectLeaveRevisorId")
    @Expose
    private int projectLeaveRevisorId;
    private final static long serialVersionUID = 3339185388106077198L;


    public EngagementRequest() {
    }

    public EngagementRequest(List<Integer> projectManagersIds, int projectLeaveRevisorId) {
        super();
        this.projectManagersIds = projectManagersIds;
        this.projectLeaveRevisorId = projectLeaveRevisorId;
    }

    public List<Integer> getProjectManagersIds() {
        return projectManagersIds;
    }

    public void setProjectManagersIds(List<Integer> projectManagersIds) {
        this.projectManagersIds = projectManagersIds;
    }

    public int getProjectLeaveRevisorId() {
        return projectLeaveRevisorId;
    }

    public void setProjectLeaveRevisorId(int projectLeaveRevisorId) {
        this.projectLeaveRevisorId = projectLeaveRevisorId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("projectManagersIds", projectManagersIds).append("projectLeaveRevisorId", projectLeaveRevisorId).toString();
    }

}