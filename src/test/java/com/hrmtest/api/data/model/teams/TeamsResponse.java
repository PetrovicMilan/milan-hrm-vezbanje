package com.hrmtest.api.data.model.teams;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;
import com.hrmtest.api.data.model.common.ErrorResponse;

public class TeamsResponse extends ErrorResponse implements Serializable {

    @SerializedName("currentPage")
    @Expose
    private Integer currentPage;
    @SerializedName("totalItems")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer totalItems;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("pageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("pageItems")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private List<PageItem> pageItems = null;


    public TeamsResponse() {
    }


    public TeamsResponse(Integer currentPage, Integer totalItems, Integer totalPages, Integer pageSize, List<PageItem> pageItems) {
        super();
        this.currentPage = currentPage;
        this.totalItems = totalItems;
        this.totalPages = totalPages;
        this.pageSize = pageSize;
        this.pageItems = pageItems;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<PageItem> getPageItems() {
        return pageItems;
    }

    public void setPageItems(List<PageItem> pageItems) {
        this.pageItems = pageItems;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TeamsResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("currentPage");
        sb.append('=');
        sb.append(((this.currentPage == null)?"<null>":this.currentPage));
        sb.append(',');
        sb.append("totalItems");
        sb.append('=');
        sb.append(((this.totalItems == null)?"<null>":this.totalItems));
        sb.append(',');
        sb.append("totalPages");
        sb.append('=');
        sb.append(((this.totalPages == null)?"<null>":this.totalPages));
        sb.append(',');
        sb.append("pageSize");
        sb.append('=');
        sb.append(((this.pageSize == null)?"<null>":this.pageSize));
        sb.append(',');
        sb.append("pageItems");
        sb.append('=');
        sb.append(((this.pageItems == null)?"<null>":this.pageItems));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}