package com.hrmtest.api.data.model.account.claimsandme.common;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Claim implements Serializable
{

    @SerializedName("Type")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String Type;
    @SerializedName("Value")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String Value;
    private final static long serialVersionUID = 6538502409927958558L;

    public Claim() {
    }


    public Claim(String type, String value) {
        super();
        this.Type = type;
        this.Value = value;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        this.Type = type;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        this.Value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Type", Type).append("Value", Value).toString();
    }

}