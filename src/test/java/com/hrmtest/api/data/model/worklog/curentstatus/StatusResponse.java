package com.hrmtest.api.data.model.worklog.curentstatus;

import java.io.Serializable;
import java.util.List;

import com.hrmtest.api.annotations.ResponseRequiredField;
import com.hrmtest.api.data.model.common.ErrorResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class StatusResponse extends ErrorResponse implements Serializable
{
    @SerializedName("CurrentLoginStatusId")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer CurrentLoginStatusId;
    @SerializedName("AvailableLoginStatusIds")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private List<Integer> AvailableLoginStatusIds = null;
    private final static long serialVersionUID = -8293339670452230712L;


    public StatusResponse() {
    }


    public StatusResponse(Integer currentLoginStatusId, List<Integer> availableLoginStatusIds) {
        super();
        this.CurrentLoginStatusId = currentLoginStatusId;
        this.AvailableLoginStatusIds = availableLoginStatusIds;
    }

    public Integer getCurrentLoginStatusId() {
        return CurrentLoginStatusId;
    }

    public void setCurrentLoginStatusId(Integer currentLoginStatusId) {
        this.CurrentLoginStatusId = currentLoginStatusId;
    }

    public List<Integer> getAvailableLoginStatusIds() {
        return AvailableLoginStatusIds;
    }

    public void setAvailableLoginStatusIds(List<Integer> availableLoginStatusIds) {
        this.AvailableLoginStatusIds = availableLoginStatusIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("CurrentLoginStatusId", CurrentLoginStatusId).append("AvailableLoginStatusIds", AvailableLoginStatusIds).toString();
    }

}
