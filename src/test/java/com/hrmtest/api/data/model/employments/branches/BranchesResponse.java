package com.hrmtest.api.data.model.employments.branches;

import java.io.Serializable;

import com.hrmtest.api.annotations.ResponseRequiredField;
import com.hrmtest.api.data.model.common.ErrorResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class BranchesResponse extends ErrorResponse implements Serializable
{
    @ResponseRequiredField(canBeEmpty = false)
    @SerializedName("BranchId")
    @Expose
    private Integer BranchId;
    @ResponseRequiredField(canBeEmpty = false)
    @SerializedName("BranchName")
    @Expose
    private String BranchName;
    private final static long serialVersionUID = 6386543729841048385L;


    public BranchesResponse() {
    }


    public BranchesResponse(Integer branchId, String branchName) {
        super();
        this.BranchId = branchId;
        this.BranchName = branchName;
    }

    public Integer getBranchId() {
        return BranchId;
    }

    public void setBranchId(Integer branchId) {
        this.BranchId = branchId;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        this.BranchName = branchName;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this).append("BranchId", BranchId).append("BranchName", BranchName).toString();
    }

}