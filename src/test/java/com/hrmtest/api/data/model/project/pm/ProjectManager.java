package com.hrmtest.api.data.model.project.pm;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProjectManager implements Serializable
{

    @SerializedName("iD")
    @Expose
    private int iD;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("isLeaveRevisor")
    @Expose
    private int isLeaveRevisor;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("to")
    @Expose
    private Object to;
    @SerializedName("workPercentage")
    @Expose
    private int workPercentage;
    @SerializedName("isActive")
    @Expose
    private boolean isActive;
    @SerializedName("imageUrl")
    @Expose
    private Object imageUrl;
    private final static long serialVersionUID = -6890646784835624646L;

    public ProjectManager() {
    }

    public ProjectManager(int iD, String fullName, int isLeaveRevisor, String from, Object to, int workPercentage, boolean isActive, Object imageUrl) {
        super();
        this.iD = iD;
        this.fullName = fullName;
        this.isLeaveRevisor = isLeaveRevisor;
        this.from = from;
        this.to = to;
        this.workPercentage = workPercentage;
        this.isActive = isActive;
        this.imageUrl = imageUrl;
    }

    public int getID() {
        return iD;
    }

    public void setID(int iD) {
        this.iD = iD;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getIsLeaveRevisor() {
        return isLeaveRevisor;
    }

    public void setIsLeaveRevisor(int isLeaveRevisor) {
        this.isLeaveRevisor = isLeaveRevisor;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Object getTo() {
        return to;
    }

    public void setTo(Object to) {
        this.to = to;
    }

    public int getWorkPercentage() {
        return workPercentage;
    }

    public void setWorkPercentage(int workPercentage) {
        this.workPercentage = workPercentage;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Object getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Object imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("iD", iD).append("fullName", fullName).append("isLeaveRevisor", isLeaveRevisor).append("from", from).append("to", to).append("workPercentage", workPercentage).append("isActive", isActive).append("imageUrl", imageUrl).toString();
    }

}