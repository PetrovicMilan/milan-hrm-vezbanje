package com.hrmtest.api.data.model.employments.engagements;

import java.io.Serializable;

import com.hrmtest.api.annotations.ResponseRequiredField;
import com.hrmtest.api.data.model.common.ErrorResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;


public class EngagementTypesResponse extends ErrorResponse implements Serializable
{

    @SerializedName("Id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer Id;
    @SerializedName("Name")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String Name;
    private final static long serialVersionUID = 7175411394991882253L;

    public EngagementTypesResponse() {
    }

    public EngagementTypesResponse(Integer id, String name) {
        super();
        this.Id = id;
        this.Name = name;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        this.Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", Id).append("Name", Name).toString();
    }

}