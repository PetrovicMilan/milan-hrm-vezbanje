package com.hrmtest.api.data.model.teams;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;

import java.io.Serializable;

public class TeamLeader implements Serializable {

    @SerializedName("Id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer id;
    @SerializedName("FirstName")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String lastName;
    @SerializedName("ParentName")
    //@ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String parentName;
    @SerializedName("Gender")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String gender;
    @SerializedName("Image")
    //@ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String image;
    @SerializedName("LoginStatusId")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer loginStatusId;
    @SerializedName("EmployeeStatus")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer employeeStatus;


    public TeamLeader() {
    }

    public TeamLeader(Integer id, String firstName, String lastName, String parentName, String gender, String image, Integer loginStatusId, Integer employeeStatus) {
        super();
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.parentName = parentName;
        this.gender = gender;
        this.image = image;
        this.loginStatusId = loginStatusId;
        this.employeeStatus = employeeStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getLoginStatusId() {
        return loginStatusId;
    }

    public void setLoginStatusId(Integer loginStatusId) {
        this.loginStatusId = loginStatusId;
    }

    public Integer getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(Integer employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TeamLeader.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("firstName");
        sb.append('=');
        sb.append(((this.firstName == null)?"<null>":this.firstName));
        sb.append(',');
        sb.append("lastName");
        sb.append('=');
        sb.append(((this.lastName == null)?"<null>":this.lastName));
        sb.append(',');
        sb.append("parentName");
        sb.append('=');
        sb.append(((this.parentName == null)?"<null>":this.parentName));
        sb.append(',');
        sb.append("gender");
        sb.append('=');
        sb.append(((this.gender == null)?"<null>":this.gender));
        sb.append(',');
        sb.append("image");
        sb.append('=');
        sb.append(((this.image == null)?"<null>":this.image));
        sb.append(',');
        sb.append("loginStatusId");
        sb.append('=');
        sb.append(((this.loginStatusId == null)?"<null>":this.loginStatusId));
        sb.append(',');
        sb.append("employeeStatus");
        sb.append('=');
        sb.append(((this.employeeStatus == null)?"<null>":this.employeeStatus));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}