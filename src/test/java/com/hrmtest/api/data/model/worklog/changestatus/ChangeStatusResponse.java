package com.hrmtest.api.data.model.worklog.changestatus;

import java.io.Serializable;
import com.hrmtest.api.common.CommonResponse;

public class ChangeStatusResponse extends CommonResponse implements Serializable
{


    public ChangeStatusResponse() {
    }

    public ChangeStatusResponse(int responseCode, String responseMessage, int data, Object specialData) {
        setResponseCode(responseCode);
        setResponseMessage(responseMessage);
        setData(data);
        setSpecialData(specialData);
    }
}