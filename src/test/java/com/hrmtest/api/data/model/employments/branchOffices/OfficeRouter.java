package com.hrmtest.api.data.model.employments.branchOffices;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class OfficeRouter implements Serializable {

    @SerializedName("OfficeRouterId")
    @Expose
    private Integer OfficeRouterId;
    @SerializedName("Title")
    @Expose
    private String Title;
    @SerializedName("MacAddress")
    @Expose
    private String MacAddress;
    private final static long serialVersionUID = -4039396566851540287L;

    public OfficeRouter() {
    }

    public OfficeRouter(Integer officeRouterId, String title, String macAddress) {
        super();
        this.OfficeRouterId = officeRouterId;
        this.Title = title;
        this.MacAddress = macAddress;
    }

    public Integer getOfficeRouterId() {
        return OfficeRouterId;
    }

    public void setOfficeRouterId(Integer officeRouterId) {
        this.OfficeRouterId = officeRouterId;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        this.Title = title;
    }

    public String getMacAddress() {
        return MacAddress;
    }

    public void setMacAddress(String macAddress) {
        this.MacAddress = macAddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("OfficeRouterId", OfficeRouterId).append("Title", Title)
                .append("MacAddress", MacAddress).toString();
    }

}