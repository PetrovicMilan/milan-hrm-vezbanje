package com.hrmtest.api.data.model.account.claimsandme.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.data.model.common.ErrorResponse;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;

public class ClaimsAndMe extends ErrorResponse implements Serializable
{

    @SerializedName("PersonId")
    @Expose
    private Integer PersonId;
    @SerializedName("EmploymentId")
    @Expose
    private Integer EmploymentId;
    @SerializedName("Name")
    @Expose
    private String Name;
    @SerializedName("Email")
    @Expose
    private String Email;
    @SerializedName("ImagesUrls")
    @Expose
    private com.hrmtest.api.data.model.account.claimsandme.common.ImagesUrls ImagesUrls;
    @SerializedName("BranchId")
    @Expose
    private Integer BranchId;
    @SerializedName("RoleId")
    @Expose
    private Integer RoleId;
    @SerializedName("Claims")
    @Expose
    private List<Claim> Claims = null;
    @SerializedName("Gender")
    @Expose
    private String Gender;
    @SerializedName("LoginStatusId")
    @Expose
    private Integer LoginStatusId;
    @SerializedName("EmployeeStatus")
    @Expose
    private Integer EmployeeStatus;
    private final static long serialVersionUID = -7223419952380531167L;

    public ClaimsAndMe() {
    }

    public ClaimsAndMe(Integer personId, Integer employmentId, String name, String email, ImagesUrls imagesUrls, Integer branchId, Integer roleId, List<Claim> claims, String gender, Integer loginStatusId, Integer employeeStatus) {
        super();
        this.PersonId = personId;
        this.EmploymentId = employmentId;
        this.Name = name;
        this.Email = email;
        this.ImagesUrls = imagesUrls;
        this.BranchId = branchId;
        this.RoleId = roleId;
        this.Claims = claims;
        this.Gender = gender;
        this.LoginStatusId = loginStatusId;
        this.EmployeeStatus = employeeStatus;
    }

    public Integer getPersonId() {
        return PersonId;
    }

    public void setPersonId(Integer personId) {
        this.PersonId = personId;
    }

    public Integer getEmploymentId() {
        return EmploymentId;
    }

    public void setEmploymentId(Integer employmentId) {
        this.EmploymentId = employmentId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public ImagesUrls getImagesUrls() {
        return ImagesUrls;
    }

    public void setImagesUrls(ImagesUrls imagesUrls) {
        this.ImagesUrls = imagesUrls;
    }

    public Integer getBranchId() {
        return BranchId;
    }

    public void setBranchId(Integer branchId) {
        this.BranchId = branchId;
    }

    public Integer getRoleId() {
        return RoleId;
    }

    public void setRoleId(Integer roleId) {
        this.RoleId = roleId;
    }

    public List<Claim> getClaims() {
        return Claims;
    }

    public void setClaims(List<Claim> claims) {
        this.Claims = claims;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        this.Gender = gender;
    }

    public Integer getLoginStatusId() {
        return LoginStatusId;
    }

    public void setLoginStatusId(Integer loginStatusId) {
        this.LoginStatusId = loginStatusId;
    }

    public Integer getEmployeeStatus() {
        return EmployeeStatus;
    }

    public void setEmployeeStatus(Integer employeeStatus) {
        this.EmployeeStatus = employeeStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("PersonId", PersonId).append("EmploymentId", EmploymentId).append("Name", Name).append("Email", Email).append("ImagesUrls", ImagesUrls).append("BranchId", BranchId).append("RoleId", RoleId).append("Claims", Claims).append("Gender", Gender).append("LoginStatusId", LoginStatusId).append("EmployeeStatus", EmployeeStatus).toString();
    }

}
