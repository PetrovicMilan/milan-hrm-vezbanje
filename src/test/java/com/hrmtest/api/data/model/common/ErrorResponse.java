package com.hrmtest.api.data.model.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class ErrorResponse implements Serializable {

    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;
    private final static long serialVersionUID = -3262874716504970523L;

    public ErrorResponse() {
    }

    public ErrorResponse(String errorMessage) {
        super();
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("errorMessage", errorMessage).toString();
    }


}
