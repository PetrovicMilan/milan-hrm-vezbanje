package com.hrmtest.api.data.model.project.pm;

import java.io.Serializable;

import com.hrmtest.api.common.CommonResponse;


public class EngagementResponse extends CommonResponse implements Serializable {


    public EngagementResponse() {
    }

    public EngagementResponse(int responseCode, String responseMessage, int data, SpecialData specialData) {
        setResponseCode(responseCode);
        setResponseMessage(responseMessage);
        setData(data);
        setSpecialData(specialData);
    }


}
