package com.hrmtest.api.data.model.teams;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;

public class PageItem implements Serializable {

    @SerializedName("Id")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer id;
    @SerializedName("Title")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private String title;
    @SerializedName("ActiveTeamMembersCount")
    @ResponseRequiredField(canBeEmpty = false)
    @Expose
    private Integer activeTeamMembersCount;
    @SerializedName("TeamLeaders")
   //@ResponseRequiredField(canBeEmpty = false)
    @Expose
    private List<TeamLeader> teamLeaders = null;
    @SerializedName("Branches")
    //@ResponseRequiredField(canBeEmpty = false)
    @Expose
    private List<Branch> branches = null;


    public PageItem() {
    }

    public PageItem(Integer id, String title, Integer activeTeamMembersCount, List<TeamLeader> teamLeaders, List<Branch> branches) {
        super();
        this.id = id;
        this.title = title;
        this.activeTeamMembersCount = activeTeamMembersCount;
        this.teamLeaders = teamLeaders;
        this.branches = branches;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getActiveTeamMembersCount() {
        return activeTeamMembersCount;
    }

    public void setActiveTeamMembersCount(Integer activeTeamMembersCount) {
        this.activeTeamMembersCount = activeTeamMembersCount;
    }

    public List<TeamLeader> getTeamLeaders() {
        return teamLeaders;
    }

    public void setTeamLeaders(List<TeamLeader> teamLeaders) {
        this.teamLeaders = teamLeaders;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(PageItem.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("title");
        sb.append('=');
        sb.append(((this.title == null)?"<null>":this.title));
        sb.append(',');
        sb.append("activeTeamMembersCount");
        sb.append('=');
        sb.append(((this.activeTeamMembersCount == null)?"<null>":this.activeTeamMembersCount));
        sb.append(',');
        sb.append("teamLeaders");
        sb.append('=');
        sb.append(((this.teamLeaders == null)?"<null>":this.teamLeaders));
        sb.append(',');
        sb.append("branches");
        sb.append('=');
        sb.append(((this.branches == null)?"<null>":this.branches));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}