package com.hrmtest.api.data.model.account.claimsandme.common;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.annotations.ResponseRequiredField;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ImagesUrls implements Serializable
{

    @SerializedName("lg_thumb")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String lg_thumb;
    @SerializedName("md_thumb")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String md_thumb;
    @SerializedName("sm_thumb")
    @ResponseRequiredField(canBeEmpty = true)
    @Expose
    private String sm_thumb;
    private final static long serialVersionUID = -6752260951961081904L;


    public ImagesUrls() {
    }


    public ImagesUrls(String lg_thumb, String md_thumb, String sm_thumb) {
        super();
        this.lg_thumb = lg_thumb;
        this.md_thumb = md_thumb;
        this.sm_thumb = sm_thumb;
    }

    public String getLg_thumb() {
        return lg_thumb;
    }

    public void setLg_thumb(String lg_thumb) {
        this.lg_thumb = lg_thumb;
    }

    public String getMd_thumb() {
        return md_thumb;
    }

    public void setMd_thumb(String md_thumb) {
        this.md_thumb = md_thumb;
    }

    public String getSm_thumb() {
        return sm_thumb;
    }

    public void setSm_thumb(String sm_thumb) {
        this.sm_thumb = sm_thumb;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("lg_thumb", lg_thumb).append("md_thumb", md_thumb).append("sm_thumb", sm_thumb).toString();
    }

}