package com.hrmtest.api.data.model.worklog.changestatus;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hrmtest.api.data.model.common.Statuses;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ChangeStatusRequest implements Serializable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("additionalInfo")
    @Expose
    private String additionalInfo;
    private final static long serialVersionUID = 1885240251280352742L;

    public ChangeStatusRequest() {
    }

    public ChangeStatusRequest(String status, String additionalInfo) {
        super();
        this.status = status;
        this.additionalInfo = additionalInfo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("additionalInfo", additionalInfo).toString();
    }

}