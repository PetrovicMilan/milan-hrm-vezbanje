package com.hrmtest.api.data.model.account.permisionsandme;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Tutorial implements Serializable
{

    @SerializedName("Id")
    @Expose
    private Integer Id;
    @SerializedName("IsFinished")
    @Expose
    private Boolean IsFinished;
    @SerializedName("TutorialType")
    @Expose
    private Integer TutorialType;
    private final static long serialVersionUID = 8297003739330109229L;


    public Tutorial() {
    }

    public Tutorial(Integer Id, boolean isFinished, int tutorialType) {
        super();
        this.Id = Id;
        this.IsFinished = isFinished;
        this.TutorialType = tutorialType;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Boolean isIsFinished() {
        return IsFinished;
    }

    public void setIsFinished(Boolean isFinished) {
        this.IsFinished = isFinished;
    }

    public Integer getTutorialType() {
        return TutorialType;
    }

    public void setTutorialType(Integer tutorialType) {
        this.TutorialType = tutorialType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", Id).append("IsFinished", IsFinished)
                .append("TutorialType", TutorialType).toString();
    }

}
