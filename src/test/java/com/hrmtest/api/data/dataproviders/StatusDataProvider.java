package com.hrmtest.api.data.dataproviders;

import com.hrmtest.api.data.model.common.Statuses;
import com.hrmtest.api.data.model.worklog.changestatus.ChangeStatusRequest;
import org.testng.annotations.DataProvider;

public class StatusDataProvider {

    @DataProvider(name = "changeStatus")
    public static Object[][] changeStatus() {
        return new Object[][]{
                // TODO Separate all statuses. Make a separate test for each status
                {
                        "ToOff",
                        new ChangeStatusRequest(Statuses.UserStatuses.OFF.toString(), "Change status to off")
                },
                {
                        "ToWorkFromHome",
                        new ChangeStatusRequest(Statuses.UserStatuses.WORK_FROM_HOME.toString(), "Change status to wfh")
                },
                {       "ToOff",
                        new ChangeStatusRequest(Statuses.UserStatuses.OFF.toString(), "Change status to off")
                },
                {       "ToWorkFromField",
                        new ChangeStatusRequest(Statuses.UserStatuses.WORK_FROM_FIELD.toString(), "Change status to wff")
                },
                {       "ToOff",
                        new ChangeStatusRequest(Statuses.UserStatuses.OFF.toString(), "Change status to off2")
                },
                {       "ToOn",
                        new ChangeStatusRequest(Statuses.UserStatuses.ON.toString(), "Change status to on")
                }
        };
    }
}


