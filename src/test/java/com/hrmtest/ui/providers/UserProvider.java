package com.hrmtest.ui.providers;


import org.testng.annotations.DataProvider;

public class UserProvider {

    @DataProvider(name = "data-provider")
    public static Object[][] profileElements() {
        return new Object[][]{
                {"profilePageContainer"}, {"FullName"}, {"Phone"},
                {"Skype"}, {"PastExperience"}, {"HtecExperience"},
                {"HtecEmail"}, {"Branch"}, {"UsualTimeInOffice"},
                {"Gender"}, {"Roles"}, {"Departments"},
                {"Positions"}, {"CPLevel"}, {"Teams"},
                {"Projects"}, {"Mentor"}, {"TL"},
                {"Superior"}, {"DateOfBirth"}, {"PersonalId"},
                {"StartOfEmployment"}, {"IDCardNumber"}, {"HtecIDCard"},
                {"PassportNumber"}, {"PrivateEmail"}, {"PrivatePhone"},
                {"EmergencyContactName"}, {"EmergencyContactNumber"}, {"EmergencyContactRelation"},
                {"RegisteredAdress"}, {"ResidentialAddress"}, {"Children"},
                {"SaintDay"}, {"Allergies"}, {"Diet"},
                {"Dissability"}, {"Glasses"}, {"ShirtSize"},
        };
    }
}
