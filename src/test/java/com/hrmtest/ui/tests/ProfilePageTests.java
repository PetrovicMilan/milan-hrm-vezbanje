package com.hrmtest.ui.tests;

import com.hrmtest.ui.pages.LoginPage;
import com.hrmtest.ui.providers.UserProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ProfilePageTests {
    private WebDriver driver;
    private Properties locators;
    private WebDriverWait waiter;


    @BeforeClass
    public void setup() throws IOException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Marko Miljkovic\\IdeaProjects\\chromedriver.exe");
        this.driver = new ChromeDriver();
        this.locators = new Properties();
        locators.load(new FileInputStream("C:\\Users\\Marko Miljkovic\\IdeaProjects\\HRM-Automation\\src\\test\\java\\com\\hrmtest\\ui\\config/locators.properties"));

        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.navigate().to(this.locators.getProperty("URL_Dev"));

    }

    @AfterClass
    public void afterClass() {
        this.driver.close();
    }


    @Test
    public void LoginTest() {
        SoftAssert sa = new SoftAssert();
        LoginPage lp = new LoginPage(driver, locators, waiter);
// Store the current window handle
        String winHandleBefore = driver.getWindowHandle();
        lp.getLoginBtn().click();

// Switch to new window opened
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }

        lp.getEmail().sendKeys(locators.getProperty("usernames"));
        lp.getEmailNextBtn().click();
        lp.getPass().sendKeys(locators.getProperty("pass"));
        lp.getPassNextBtn().click();

        driver.switchTo().window(winHandleBefore);
        sa.assertEquals(lp.isEntered(), true);
    }

    @Test(dataProvider = "data-provider", dataProviderClass = UserProvider.class)
    public void ValidateElements(String data) {
        boolean present;
        try {
            driver.findElement(By.xpath(this.locators.getProperty(data)));
            present = true;
        } catch (NoSuchElementException e) {
            present = false;
        }
    }
}
