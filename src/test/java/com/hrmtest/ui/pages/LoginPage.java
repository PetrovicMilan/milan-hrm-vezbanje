package com.hrmtest.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Properties;

public class LoginPage {
    private WebDriver driver;
    private Properties locators;
    private WebDriverWait waiter;

    public LoginPage(WebDriver driver, Properties locators, WebDriverWait waiter) {
        this.driver = driver;
        this.locators = locators;
        this.waiter = waiter;
    }

    public WebElement getLoginBtn() {
        return this.driver.findElement(By.xpath(this.locators.getProperty("login_btn")));
    }
    public WebElement getEmail() {
        return this.driver.findElement(By.xpath(this.locators.getProperty("email_input")));
    }
public void setEmail(String email){
        this.getEmail().clear();
        this.getEmail().sendKeys(email);
}
    public WebElement getEmailNextBtn() {
        return this.driver.findElement(By.xpath(this.locators.getProperty("next_email_btn")));
    }
    public WebElement getPass() {
        return this.driver.findElement(By.xpath(this.locators.getProperty("pass_input")));
    }
    public void setPass(String pass){
        this.getPass().clear();
        this.getPass().sendKeys(pass);
    }
    public WebElement getPassNextBtn() {
        return this.driver.findElement(By.xpath(this.locators.getProperty("next_pass_btn")));
    }
    public void enterSite() {
        this.getPassNextBtn().click();
    }
    public boolean isEntered() {
        boolean isEntered;
        try {
            this.driver.findElement(By.xpath(this.locators.getProperty("full_name")));
             isEntered = true;
        } catch (Exception e) {
            isEntered = false;
        }
        return isEntered;
    }
}